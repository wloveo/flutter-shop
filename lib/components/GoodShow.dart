import 'package:flutter/material.dart';


class GoodShowPage extends StatefulWidget {

  final String title;
  final String goodUrl;
  final int id;

  GoodShowPage({Key key,this.title,this.goodUrl,this.id}) : super(key: key);

  @override
  _GoodShowPageState createState() => _GoodShowPageState();
}

class _GoodShowPageState extends State<GoodShowPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: InkWell(
        child: Column(
            children: [
            Image.network(widget.goodUrl,fit: BoxFit.cover),
            Text(widget.title,maxLines: 1,overflow:TextOverflow.ellipsis),
            SizedBox(height:10),
            Row(
              children: [
                Expanded(child: Text("已售：20件"),flex: 1),
                Expanded(child: Text("￥24.5",textDirection: TextDirection.rtl,style: TextStyle(color: Colors.red),),flex: 1),
              ]
            )
          ],
        ),
        onTap: () {
          print("点击了商品: ${widget.id}");
          Navigator.pushNamed(context, "/goodDetail",arguments:widget.id);
        },
      ),
    );
  }
}