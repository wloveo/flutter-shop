import 'package:flutter/material.dart';

import 'GoodShow.dart';

class GoodListPage extends StatefulWidget {
  final List goods;
  final String title;
  final IconData incos;


  GoodListPage({Key key, this.goods,this.title,this.incos}) : super(key: key);

  @override
  _GoodListPageState createState() => _GoodListPageState();
}

class _GoodListPageState extends State<GoodListPage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
          child: Row(
          children: [
            Expanded(
              child: Text(""),
              flex: 1,
            ),
            Icon(widget.incos),
            Text("${widget.title}", style: TextStyle()),
            Expanded(
              child: Text(""),
              flex: 1,
            ),
          ],
        )),
        Container(
          padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
          child: Column(
            children: [
              Row(children: [
                Expanded(
                  child: GoodShowPage(
                    goodUrl:
                        'https://img10.360buyimg.com/n7/jfs/t1/132583/19/3043/112558/5ef55db1E2fdf9431/8f9ff5efcdec026b.jpg',
                    title: '特步男鞋运动鞋男跑步鞋子男夏季网面透',
                    id: 1
                  ),
                  flex: 1,
                ),
                SizedBox(width: 10),
                Expanded(
                  child: GoodShowPage(
                    goodUrl:
                        'https://img10.360buyimg.com/n7/jfs/t1/161388/22/9161/214589/603f41b6Ef990d8fa/8507606427175001.jpg',
                    title: '麟零（LIN LING）跑步鞋男春季新款莆',
                    id: 2,
                  ),
                  flex: 1,
                ),
              ]),
              SizedBox(height: 10),
              Row(children: [
                Expanded(
                  child: GoodShowPage(
                    goodUrl:
                        'https://img10.360buyimg.com/n6/jfs/t13468/231/2492466370/184103/15a82275/5a7a8eadN3a3ae6e4.jpg.webp',
                    title: '上善若水 斗战胜佛之静思 纯铜齐天大圣',
                    id: 3,
                  ),
                  flex: 1,
                ),
                SizedBox(width: 10),
                Expanded(
                  child: GoodShowPage(
                    goodUrl:
                        'https://img12.360buyimg.com/babel/s320x320_jfs/t1/3262/37/12871/472669/5bd6ac33Ea3730aa2/e39037efaa0cc562.jpg!cc_320x320.webp',
                    title: '瑜伽服女套装新款秋冬季运动时尚休闲跑步健身套装五件套修身显瘦 黑色 M',
                    id:4
                  ),
                  flex: 1,
                ),
              ]),
            ],
          ),
        )
      ],
    );
  }
}
