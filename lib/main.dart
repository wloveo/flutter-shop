import 'package:flutter/material.dart';

import 'pages/toolbar/Toolbar.dart';
import 'routers/AppRouter.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new Scaffold(
        bottomNavigationBar:  Toolbar()
      ),
      initialRoute: '/',
      onGenerateRoute: AppRouter().getRoutes
    );
  }
}
