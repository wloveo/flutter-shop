import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';


class UserCenterPage extends StatefulWidget {
  UserCenterPage({Key key}) : super(key: key);

  @override
  _UserCenterPageState createState() => _UserCenterPageState();
}

class _UserCenterPageState extends State<UserCenterPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(children: [
        Container(
            alignment: Alignment.center,
            padding: EdgeInsets.fromLTRB(0, 50, 0, 0),
            height: 200,
            color: Colors.red,
            width: double.infinity,
            child: Column(
              children: [
                ClipOval(
                  child: Image.asset(
                    "images/head.jpg",
                    height: 100,
                  ),
                ),
                SizedBox(height: 10),
                Text(
                  "大美女王大治",
                  style: TextStyle(color: Colors.white),
                )
              ],
            )
        ),
        Container(
            child: Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: this._getListData()
                ),
              ),
            )
        )
      ]),
    );
  }

  _showToast(String msg) {
    Fluttertoast.showToast(
          msg: msg,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
  }

  List<Widget> _getListData() {
    return [
                ListTile(
                  leading: Icon(Icons.menu),
                  title: Text("我的订单"),
                  trailing:  Icon(Icons.keyboard_arrow_right),
                  onTap: () => _showToast("我的订单"),
                ),
                Divider(),
                ListTile(
                  leading: Icon(Icons.settings),
                  title: Text("我的设置"),
                  trailing:  Icon(Icons.keyboard_arrow_right),
                  onTap: () => _showToast("我的设置"),
                ),
                Divider(),
                ListTile(
                  leading: Icon(Icons.money),
                  title: Text("账单信息"),
                  trailing:  Icon(Icons.keyboard_arrow_right),
                  onTap: () => _showToast("账单信息"),
                ),
                Divider(),
                ListTile(
                  leading: Icon(Icons.logout),
                  title: Text("退出登录"),
                  trailing:  Icon(Icons.keyboard_arrow_right),
                  onTap: () => _showToast("退出登录"),
                ),
                Divider(),
                ListTile(
                  leading: Icon(Icons.money),
                  title: Text("更多设置"),
                  trailing:  Icon(Icons.keyboard_arrow_right),
                  onTap: () => _showToast("更多设置"),
                ),
                Divider(),
                ListTile(
                  leading: Icon(Icons.logout),
                  title: Text("敬请期待"),
                  trailing:  Icon(Icons.keyboard_arrow_right),
                  onTap: () => _showToast("敬请期待"),
                ),
                Divider(),
                ListTile(
                  leading: Icon(Icons.logout),
                  title: Text("显示更多"),
                  trailing:  Icon(Icons.keyboard_arrow_right),
                  onTap: () => _showToast("显示更多"),
                ),
                Divider(),
                ListTile(
                  leading: Icon(Icons.logout),
                  title: Text("商城协议"),
                  trailing:  Icon(Icons.keyboard_arrow_right),
                  onTap: () => _showToast("商城协议"),
                ),
                Divider(),
              ];
  }
}
