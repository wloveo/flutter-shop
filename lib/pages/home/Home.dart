import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import '../../components/CategoryIcon.dart';
import '../../components/GoodList.dart';

// ignore: must_be_immutable
class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  List banners = [
    "https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2758014252,3925245251&fm=15&gp=0.jpg",
    "https://pic2.zhimg.com/v2-f31b881b2fe0c52148b145612fbbdda8_r.jpg",
    "https://pic4.zhimg.com/v2-8c2096d18b077856b3a291f624d6107d_r.jpg?source=1940ef5c"
  ];

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("主页"),
        ),
        body: SingleChildScrollView(
          
            child: Column(children: [
          Container(
              padding: EdgeInsets.all(10),
              height: 200,
              child: Swiper(
                itemCount: 3,
                itemBuilder: (context, index) {
                  return Image.network(widget.banners[index], fit: BoxFit.fill);
                },
                pagination: SwiperPagination(),
                // control: SwiperControl(),
                autoplay: true,
              )),
          Container(
            padding: EdgeInsets.all(10),
            height: 200,
            child: Wrap(
              spacing: 40.0, // 主轴(水平)方向间距
              runSpacing: 25.0, // 纵轴（垂直）方向间距
              alignment: WrapAlignment.start, //模式
              children:_getCategory(),
            ),
          ),
          Container(
            child: GoodListPage(title: "热销商品",incos: Icons.hot_tub,)
          ),
          Container(
            child: GoodListPage(title: "最新商品",incos: Icons.public,)
          )
        ])
      )
    );
  }

  List<Widget> _getCategory() {
    return  [
                CategoryIconPage(
                  name: '帽子',
                  imageUrl: 'images/icons/fushi--caomao.png',
                  id:1,
                ),
                CategoryIconPage(
                  name: '上衣',
                  imageUrl: 'images/icons/fushi--chenshan.png',
                  id:2,
                ),
                CategoryIconPage(
                  name: '短裤',
                  imageUrl: 'images/icons/fushi--duanku.png',
                  id:3,
                ),
                CategoryIconPage(
                  name: '短裙',
                  imageUrl: 'images/icons/fushi--duanqun.png',
                  id:4,
                ),
                CategoryIconPage(
                  name: '袜子',
                  imageUrl: 'images/icons/fushi--wazi.png',
                  id:5,
                ),
                CategoryIconPage(
                  name: '鞋子',
                  imageUrl: 'images/icons/fushi--xie.png',
                  id:6,
                ),
                CategoryIconPage(
                  name: '运动裤',
                  imageUrl: 'images/icons/fushi--yundongku.png',
                  id:7,
                ),
            ];
  }

}
