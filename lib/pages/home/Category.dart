
import 'package:flutter/material.dart';

import '../category/CategoryInfo.dart';

class CategoryPage extends StatefulWidget {
  CategoryPage({Key key}) : super(key: key);

  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("分类"),
      ),
      body: Container(
        child: new CategoryInfoPage(),
      ),
    );
  }
}